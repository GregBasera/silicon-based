import numpy as npy
import matplotlib.pyplot as mplot

def getDataset(filename, p):
    # load the dataset from textfile
    data = npy.loadtxt(filename)

    # include bias to the dataset for easy processing
    dataRows, dataCols = data.shape
    bias = npy.ones((dataRows, 1))
    data = npy.hstack((bias, data))

    # Normalization confuses me. I know how to do it, but i prefer not

    # data split to train and test
    split_factor = p
    split = int(split_factor * data.shape[0])

    train = data[:split,:]
    test = data[split:,:]

    # seperate the y of both datasets for later
    y_train = npy.reshape(train[:,dataCols], (train.shape[0],1))
    train = train[:,:-1]
    y_test = npy.reshape(test[:,dataCols], (test.shape[0],1))
    test = test[:,:-1]

    return train, y_train, test, y_test

def regress(thetas, data, dataRows, dataCols):
    h = npy.zeros((dataRows,1))

    for q in range(dataRows):
        for w in range(dataCols):
            h[q,0] = h[q,0] + (thetas[w] * data[q,w])

    return h

def getCost(h, y, dataRows):
    sum = 0

    for q in range(dataRows):
        sum = sum + (h[q][0] - y[q][0]) ** 2

    return sum / (2 * dataRows)

def gDescend(thetas, a, h, y, data, dataRows):
    temp = npy.zeros(thetas.shape)

    for q in range(thetas.shape[0]):
        sum = 0
        for w in range(dataRows):
            if w == 0:
                sum = sum + ((h[q][0] - y[q][0]))
            else:
                sum = sum + ((h[q][0] - y[q][0]) * data[w,q])
        temp[q] = thetas[q] - (a * (sum / dataRows))

    return temp

# =================================================================================
# =================================================================================

# data, dataRows, dataCols, y, avg, rng = getDataset()
train, y_train, test, y_test = getDataset("Real-estate-valuation-data-set.txt", 0.80)
a = 0.000000001
thetas = npy.zeros(train.shape[1])

# Initial costs
h = regress(thetas, train, train.shape[0], train.shape[1])
train_i_cost = getCost(h, y_train, train.shape[0])
h = regress(thetas, test, test.shape[0], test.shape[1])
test_i_cost = getCost(h, y_test, test.shape[0])

# Train with training set
h = regress(thetas, train, train.shape[0], train.shape[1])
prev = getCost(h, y_train, train.shape[0])
thetas = gDescend(thetas, a, h, y_train, train, train.shape[0])
h = regress(thetas, train, train.shape[0], train.shape[1])
next = getCost(h, y_train, train.shape[0])
print(prev - next)
while (prev - next) > 0.0000001:
    prev = next
    thetas = gDescend(thetas, a, h, y_train, train, train.shape[0])
    h = regress(thetas, train, train.shape[0], train.shape[1])
    next = getCost(h, y_train, train.shape[0])
    print(prev - next)

# Final costs
h = regress(thetas, train, train.shape[0], train.shape[1])
train_f_cost = getCost(h, y_train, train.shape[0])
h = regress(thetas, test, test.shape[0], test.shape[1])
test_f_cost = getCost(h, y_test, test.shape[0])

# Results
print()
print('Train-dataset shape:     ', train.shape)
print('Test-dataset shape:      ', test.shape)
print('======================================================')
print('Train-set initial Cost:      %f' % train_i_cost)
print('Train-set final Cost:        %f' % train_f_cost)
print('Test-set initial Cost:       %f' % test_i_cost)
print('Test-set final Cost:         %f' % test_f_cost)
print('======================================================')
print('Output:')
print(thetas)

# mplot.show()
