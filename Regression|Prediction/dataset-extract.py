import numpy as npy

data = npy.loadtxt("Real estate valuation data set.txt")
data = data[:,2:]

f = open("Real estate valuation data set.txt", "w")
row, col = data.shape
for q in range(row):
    for w in range(col):
        f.write('%f ' % data[q,w])
    f.write('\n')
f.close()
