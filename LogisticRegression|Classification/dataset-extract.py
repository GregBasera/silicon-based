import numpy as npy

data = npy.loadtxt("breast-cancer-wisconsin.txt")
data = data[:,2:]

f = open("breast-cancer-wisconsin.txt", "w")
row, col = data.shape
for q in range(row):
    for w in range(1,col):
        f.write('%f ' % data[q,w])
    f.write('\n')
f.close()
