# Notice:
---
* This algoritm only works for two classifications **(1 or 0)**. If you want to use this to classify multiple classifications, you'll have to use the **one-vs-rest** method.
  * Suppose you want to classify between *y = 1 || 2 || 3 || 4*
  * you'll have to create a artificial dataset where *y = 1 || 0 || 0 || 0*
  * Repeat that on all the values of y and you'll end up with 4 hypotheses.
  * As you get a new set of x, you can then run it by these 4 hypotheses and pick which one produces the most confident result.
  * **Or you could just use Artificial Neural Networks, I think it performs better for multi-class classification**
