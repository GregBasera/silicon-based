# Project
---
| Name | MachineLearning |
|---|---|
| Description | A culmination of the things I learned in the area of MachineLearning. |
| Author | Basera, Greg Emerson A. (gbasera@gbox.adnu.edu.ph) |

# Environment
---
* Linux
* Python
* Numpy
* Matplotlib

# Disclaimer
---
1. A lot of the algorithms included in here are in the novice level.
2. You'll see a lot of Gradient Descend (a simple optimization algorithm with a decent performance rating, but is not considered the best in the Machine Learning world).
3. I programmed most of these algorithms **Functionally**, not **Objectively**.
4. Due to item.3 most of these are not optimized for multi-threaded computing.
5. I made this project with the purpose of learning. It's never meant to be the most "effiecient" :(

### Normalization
* **Formula:** n_i = (n_i * average) / n_i.max - n_i.min
* Normalization is a data pre-processing technique used to scale the dataset smaller.
* It's suppose to make Gradient Descent much faster, while not having any effect on the final output.
* Debugging a normalized set is very confusing for me personally hence I didn't implement it. I would recommend doing it if you deem it necessary.

### Regularization
* **Formula:**
* *Overfitting* is the tendency of a *hypothesis* to do well in training sessions but not as well in testing. This is very relevant if you have a large amount featuers or higher order hypothesis (non-linear).
* There are two ways of combating Overfitting (that I know of):
  1. Manually remove features -- You could consult an expert on the field and ask for his opinion on what must be considered in comming up with your output. Or you could get the relationship between each of your feature and the output (there's a formula for this).
  2. Regularization -- is basically a function attached to the cost fuction that will penalize the value of your *thetas*, providing for a much more linear hypothesis.
