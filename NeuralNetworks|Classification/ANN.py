import numpy as npy
import matplotlib.pyplot as mplot

def getDataset(filename, p):
    # load the dataset from textfile
    data = npy.loadtxt(filename)

    # include bias to the dataset for easy processing
    dataRows, dataCols = data.shape
    bias = npy.ones((dataRows, 1))
    data = npy.hstack((bias, data))

    # Normalization confuses me. I know how to do it, but i prefer not

    # data split to train and test
    split_factor = p
    split = int(split_factor * data.shape[0])

    train = data[:split,:]
    test = data[split:,:]

    # seperate the y of both datasets for later
    y_train = npy.reshape(train[:,dataCols], (train.shape[0],1))
    train = train[:,:-1]
    y_test = npy.reshape(test[:,dataCols], (test.shape[0],1))
    test = test[:,:-1]

    return train, y_train, test, y_test

def forwardPropagate(train, first_theta, next_theta, final_theta, hidden):
    x = npy.reshape(train[0,:], (train.shape[1], 1))

    for q in range(hidden.shape[0]):
        hidden[q,0] = 1 / (1 + (2.718281828 ** -(npy.dot(first_theta[q,:], x))))

    for q in range(1, hidden.shape[1]):
        o = next_theta[q-1]
        for w in range(hidden.shape[0]):
            hidden[w,q] = 1 / (1 + (2.718281828 ** -(npy.dot(o[w,:], hidden[:,q-1]))))

    h = npy.zeros((final_theta.shape[0], 1))
    for q in range(h.shape[0]):
        h[q,0] = 1 / (1 + (2.718281828 ** -(npy.dot(final_theta[q,:], hidden[:,hidden.shape[1]-1]))))

    return hidden, h

# =================================================================
# =================================================================

layers = int(input("Hidden Layer layers: "))
nodes = int(input("Hidden Layer nodes: "))
hidden = npy.zeros((nodes, layers))
output = 1

train, y_train, test, y_test = getDataset("breast-cancer-wisconsin.txt", 0.80)

# First_theta is different from other theta, its 2D and has alot of cols because num of inputs
first_theta = npy.random.rand(nodes, train.shape[1])
# next_theta is different, its 3D and has uniform rows and cols
next_theta = npy.random.rand(layers-1, nodes, nodes)
# final_theta is also different, its a usally a vector, depends on the desired num of output
final_theta = npy.random.rand(output, nodes)

hidden, h = forwardPropagate(train, first_theta, next_theta, final_theta, hidden)
print(hidden)
print(h)
