import numpy as npy

mean = npy.array([3.0, 3.0])
cov = npy.array([[1.0, -0.95], [-0.95, 1.2]])
data = npy.random.multivariate_normal(mean, cov, 4000)

f = open("dataset.txt", "w")
# f.write('%f %f\n' % (q, rand.uniform(1000-q-100,1000-q+100)))
row, col = data.shape
for q in range(row):
    for w in range(col):
        f.write('%f ' % data[q,w])
    f.write('\n')
f.close()
